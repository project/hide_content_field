# Hide Content Field

Hide Content Field modules provides configuration or option to hide field from
form display, field exist on a form in a hidden mode.

For a full description of the module, visit the
[project page](https://www.drupal.org/project/hide_content_field).

Submit bug reports and feature suggestions, or track changes in the
[issue queue](https://www.drupal.org/project/issues/hide_content_field).


## Requirements

This module requires no modules outside of Drupal core.


## Installation

Install as you would normally install a contributed Drupal module. For further
information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).


## Configuration

1. Enable the module using drush command
   `drush en hide_content_field`.

   OR

   Goto `Administration => Extend` and enable the module.
2. Go to Structure => Content Types => Select any content type => Manage Fields
   => click on edit any field.
3. You can see the check box named as "Hide", Click on the same check box and
   click on save button.

*Note:* This module support field of type : [string, list_string, text, email,
entity_reference, path, uri].


## Maintainers

- [Chandravilas Kute (chandravilas)](https://www.drupal.org/u/chandravilas)
